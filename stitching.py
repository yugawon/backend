import os
import cv2
import sys
import glob
import time
import logging
import numpy as np
from PIL import Image

def stitching_images(directory):

    logging.info('Stitching Images\n')
    path = 'D:\\WORKSPACE_TEST_FILE\\vr\\'+ directory +'\\'

    img_list = []
    img_list = sorted(glob.glob(path+'*.jpg'))
    
    print(img_list)

    #### get height
    mh = get_height(img_list)

    imgs = []

    mode = cv2.STITCHER_PANORAMA

    if int(cv2.__version__[0]) == 3:
      stitcher = cv2.createStitcher(mode)
    else:
      stitcher = cv2.Stitcher_create(mode)

    ### Image Resizing
    for img_path in img_list:
      img = cv2.imread(img_path)
      img = image_resize(img, height=mh)
      cv2.imwrite(img_path,img)

    ### make panorama
    for img_path in img_list:     
      img = cv2.imread(img_path)
      imgs.append(img)

    status, stitched = stitcher.stitch(imgs)

    if status == 0:
        gray = cv2.cvtColor(stitched, cv2.COLOR_BGR2GRAY)
        thresh = cv2.bitwise_not(cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY)[1])
        thresh = cv2.medianBlur(thresh, 5)

        stitched_copy = stitched.copy()
        thresh_copy = thresh.copy()

        while np.sum(thresh_copy) > 0:
            thresh_copy = thresh_copy[1:-1, 1:-1]
            stitched_copy = stitched_copy[1:-1, 1:-1]
        
        cv2.imwrite(os.path.join(path, 'result_crop.jpg'), stitched_copy)
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+str(status))

    else:
      result = cv2.hconcat(imgs) ### image concat
      cv2.imwrite(os.path.join(path,'result_crop.jpg'), result)
      logging.info('failed, status: {}'.format(status))
      print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! fail "+str(status))

### get image min_height
def get_height(img_list):
  height = []

  for img_path in img_list:
    image = Image.open(img_path)
    height.append(image.size[1])

  min_height = min(height)

  return min_height


### image resizing
def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):   
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image
 
    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)

    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation = inter)

    return resized
    
    