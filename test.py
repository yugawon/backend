from sqlalchemy import create_engine, text
from flask import Flask, jsonify, request

app = Flask(__name__)
app.config.from_pyfile('config.py')

database = create_engine(app.config['DB_URL'], encoding = 'utf-8')
app.database = database


@app.route('/test', methods = ['GET'])
def sign_up():
    res = app.database.execute(text("select path, id from result"))
    #print(res.fetchall())
    d = res.fetchall()
    dictionary = dict(d)
    print(dictionary)
    return dictionary


if __name__=='__main__':
    app.run(debug=True, host='114.70.186.30')