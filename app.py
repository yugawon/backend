from flask import Flask, jsonify, request, send_file, jsonify
from flask_cors import CORS
from werkzeug.utils import secure_filename
from sqlalchemy import create_engine, text
import os
from datetime import datetime
import base64
import stitching
from PIL import Image

app = Flask(__name__)
app.config.from_pyfile('config.py')

database = create_engine(app.config['DB_URL'], encoding = 'utf-8')
app.database = database

CORS(app)

### Test Page
@app.route('/') 
def test():
    return 'hello'

### get Result List 
@app.route('/getlist',methods=['POST'])
def get_list():
    id = request.form.get('id')
    print(id)
    res = app.database.execute(text("select path, id from result where id='"+id+"'"))
    d = res.fetchall()
    dictionary = dict(d)
    print(dictionary)
    return dictionary


### return panorama image
@app.route('/send', methods=['POST'])
def send_image():
    dir_name = request.get_json()
    file_name = "D:\\WORKSPACE_TEST_FILE\\vr\\" + dir_name['dir_name'] + "\\result_crop.jpg"
    
    with open(file_name, "rb") as img_file:
        result = base64.b64encode(img_file.read())
    
    return result


### make panorama image
@app.route('/upload', methods=['POST'])
def upload_files():
    index = 1
    if request.method == 'POST':
        dir_name = datetime.today().strftime('%Y-%m-%d-%H-%M-%S')
        path = os.path.join('D:\\WORKSPACE_TEST_FILE\\vr\\',dir_name)
        os.mkdir(path)
        files = request.files.getlist('file')
        id = request.form.get('id')
        print('id : '+ id)
        for f in files:
            f.save(path+'\\'+secure_filename(str(index).zfill(2)+'.jpg'))
            index+=1
        stitching.stitching_images(dir_name)
        app.database.execute(text("insert into result(id,path) values ('" +id+ "','" +dir_name+ "')"))
        url_path = "114.70.186.30:23000/url/"+dir_name
        return jsonify(url_path)


### admin page (get all user's result)
@app.route('/admin', methods=['POST'])
def get_all():
    res = app.database.execute(text("select path, id from result"))
    d = res.fetchall()
    dd = dict(d)
    print(dd)
    return dd


### return result image url
@app.route('/url/<dir_name>')
def downloadfile(dir_name):
    file_name = "D:\\WORKSPACE_TEST_FILE\\vr\\" + dir_name + "\\result_crop.jpg"
    try:
        return send_file(file_name, attachment_filename=dir_name+'.jpg')
    except Exception as e:
        return str(e)

if __name__=='__main__':
    app.run(debug=True, host='0.0.0.0', port='23000')
    